import requests




class XroadClient():

    def __init__(self, header_params, xroad_server,cert=None,key=None):
        self.header_params = header_params
        self.xroad_server = xroad_server
        self.cert=cert
        self.key=key


    def get_header(self):
        header = """<xro:protocolVersion>{protocolVersion}</xro:protocolVersion>
      <xro:id>{id}</xro:id>
      <xro:userId>{userId}</xro:userId>
      <xro:service iden:objectType="SERVICE">
         <iden:xRoadInstance>{serviceXRoadInstance}</iden:xRoadInstance>
         <iden:memberClass>{serviceMemberClass}</iden:memberClass>
         <iden:memberCode>{serviceMemberCode}</iden:memberCode>
         <iden:subsystemCode>{serviceSubsystemCode}</iden:subsystemCode>
         <iden:serviceCode>{serviceCode}</iden:serviceCode>
         <iden:serviceVersion>{serviceVersion}</iden:serviceVersion>
      </xro:service>
      <xro:client iden:objectType="SUBSYSTEM">
         <iden:xRoadInstance>{clientXRoadInstance}</iden:xRoadInstance>
         <iden:memberClass>{clientMemberClass}</iden:memberClass>
         <iden:memberCode>{clientMemberCode}</iden:memberCode>
         <iden:subsystemCode>{clientSubsystemCode}</iden:subsystemCode>
      </xro:client>""".format(**self.header_params)
        return header

    def get_body(self,params):
        body = """<kpr:laekumised_maksuametist>
         <ISIKUKOOD>{}</ISIKUKOOD>
      </kpr:laekumised_maksuametist>""".format(params)
        return body

    def compose_message(self,params):
        message = """
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xro="http://x-road.eu/xsd/xroad.xsd" xmlns:iden="http://x-road.eu/xsd/identifiers" xmlns:kpr="http://producers.kpr.x-road.eu/producer/kpr">
        <soapenv:Header>
        {}
        </soapenv:Header>
        <soapenv:Body>
        {}
        </soapenv:Body>
        </soapenv:Envelope>
        """.format(self.get_header(),self.get_body(params))
        return message

    def make_query(self, params):
        message = self.compose_message(params)
        headers = {'Content-type': 'text/xml'}
        certs=(self.cert,self.key)
        resp = requests.post(self.xroad_server, data=message, headers=headers,
                            cert=certs,
                            verify=False)
        return resp.text






