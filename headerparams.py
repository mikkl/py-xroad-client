import uuid

#https://www.x-tee.ee/docs/live/xroad/pr-mess_x-road_message_protocol.html#22-message-headers -info about x road headers
#https://test.x-tee.ee/catalogue/ee-dev - find your x-road wsdls
XROAD_HEADER_PARAMS = {'protocolVersion': '4.0', 'id': uuid.uuid4(), 'userId': '',
                       'clientXRoadInstance': "ee-dev", "clientMemberClass": "COM", "clientMemberCode": "",
                       "clientSubsystemCode": "", 'serviceXRoadInstance': "ee-dev",
                       "serviceMemberClass": "COM", "serviceMemberCode": "14282597",
                       "serviceSubsystemCode": "kpr", "serviceCode": "laekumised_maksuametist", "serviceVersion": "v1"
                       }
