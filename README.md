# py-xroad client

py-xroad client is a Python library for making requests to xroad. This libary is created to simplify integration with xroad, since there is supported libary in java but not in other languages. Not this is the example implementation.

## Installation

Clone the git with git clone command.


## Usage

```
from headerparams import XROAD_HEADER_PARAMS
from xroad_client import XRoadHelper

xroad_server_url = "YOUR_SECURITY_SERVER_URL"
xc = XroadClient(XROAD_HEADER_PARAMS, xroad_server_url)
resp = xc.make_query("386111723")
print(resp)
```
Replace xroad headerparams in headerparams.py with **your own** header parameters.

Currently supports making query to Pensionikeskus. When other queries are needed just replace contents of body with needed parameters

## Connection
py-xroad supports connecting over ssh using tls just add certificate and key location like this.
```
xc = XroadClient(XROAD_HEADER_PARAMS, xroad_server_url, cert="",key="")
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)